import { useState } from 'react'
import './style.css'

function GetUserComponent({ setUser, setLoggedIn }) {

    const [userInput, setUserInput] = useState("")

    const handleLogin = (userInput) => {
        setUser(userInput)
        setLoggedIn(true)
    }

    return (
        <>
            <form className="flex">
                <input className="flex__input" placeholder="Username" type="text" value={userInput}
                    onChange={(event) => setUserInput(event.target.value)} />
                <button onClick={() => handleLogin(userInput)}>Acessar com o nome</button>
            </form>
        </>

    )
}

export default GetUserComponent