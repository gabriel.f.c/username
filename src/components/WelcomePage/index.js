function WelcomePage({ user, setLoggedIn }) {

    const handleLogout = (setLoggedIn) => {
        setLoggedIn(false)
    }

    return (
        <div>
            <h1>Welcome, {user}</h1>
            <button onClick={() => handleLogout(setLoggedIn)}>Logout</button>
        </div>
    )


}

export default WelcomePage